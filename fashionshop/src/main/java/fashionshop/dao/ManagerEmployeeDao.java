package fashionshop.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fashionshop.model.Employee;

@Repository
@Transactional(rollbackFor = Exception.class)
public class ManagerEmployeeDao {
	private static final Logger logger = Logger.getLogger(ManagerEmployeeDao.class);
	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * public void save(NhanVien nhanVien) { Session session =
	 * sessionFactory.openSession(); try { session.save(nhanVien);
	 * logger.debug("insert oke"); logger.info("ccc"); } catch (RuntimeException e)
	 * { logger.debug("ok"); }finally { session.close();
	 * 
	 * } }
	 */
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<Employee> getAllEmployee() {
		Session session = sessionFactory.openSession();
		List<Employee> listEmployee = null;
		try {
			String sql = "select e.id, e.name,e.email,e.address,e.phone from Employee e";
			listEmployee = session.createNativeQuery(sql.toString())
					.setResultTransformer(Transformers.aliasToBean(Employee.class)).list();
			logger.debug("SQL:\n " + sql);

		} catch (RuntimeException e) {
			logger.error("runtime : " + e);
		} finally {
			session.close();
		}
		return listEmployee;
	}

	public List<Employee> findEmployee(String email) {
		Session session = sessionFactory.openSession();
		List<Employee> listEmployee = null;
		String sql = "select e.id, e.email from employee e where e.email = :email";
		listEmployee = session.createNativeQuery(sql.toString()).setParameter("email", email).getResultList();
		return listEmployee;
	}

	public void addEmployee(Employee employee) throws SQLException {
		Session session = sessionFactory.openSession();
		try {
			session.save(employee);
		} catch (RuntimeException e) {
			logger.error("error sql");
		} finally {
			session.close();
		}
	}

	public void deleteEmployee(int id) {
		Session session = sessionFactory.openSession();
		int deletedEntities;
		try {
			session.beginTransaction();
			String sql = "delete from employee where id = :id";
			deletedEntities = session.createNativeQuery(sql.toString()).setParameter("id", id).executeUpdate();
			session.getTransaction().commit();
			logger.debug("OKE");
		} catch (RuntimeException e) {
			session.getTransaction().rollback();
			logger.error("error sql");
		} finally {
			session.close();
		}
	}
	
	public void deleteSelectEmployee(List<Integer> id) {
		Session session = sessionFactory.openSession();
		List<Integer> list = new ArrayList<Integer>();
		String sql = "";
		sql = "in(";
		for(int i : id ) {
		}
	}
}
