package fashionshop.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fashionshop.model.Employee;
import fashionshop.service.ManagerEmployeeService;

@Controller
@Transactional
@RequestMapping("/manager")
public class ManagerEmployeeController {
	private static final Logger logger = Logger.getLogger(ManagerEmployeeController.class);
	@Autowired
	private ManagerEmployeeService managerEmployeeService;

	@RequestMapping("/index")
	public String test() {
		return "employee/index";
	}

	@RequestMapping("/list-employee")
	public String managerEmployee() {
		return "employee/manageremployee";
	}

	@RequestMapping("/get-all-employee")
	public @ResponseBody List<Employee> getAllEmployee() {
		return managerEmployeeService.getAllEmployee();
	}

	@RequestMapping("/add-employee")
	public @ResponseBody String addEmployee(@RequestParam("name") String name, @RequestParam("email") String email,
			@RequestParam("address") String address, @RequestParam("phone") String phone)
			throws SQLException, JsonProcessingException {
		Employee employee = new Employee(name, email, address, phone);
		List<Employee> list = new ArrayList<Employee>();
		String ObjectEmployee = "";
		if(managerEmployeeService.findEmployee(email).size() > 0) {
			ObjectMapper objectMapper = new ObjectMapper();
			ObjectEmployee = objectMapper.writeValueAsString(list);
		}else {
			managerEmployeeService.addEmployee(employee);
			list.add(employee);
			ObjectMapper objectMapper = new ObjectMapper();
			ObjectEmployee = objectMapper.writeValueAsString(list);
		}
		return ObjectEmployee;
	}
	@RequestMapping("/delete-employee")
	public @ResponseBody String deleteEmployee(@RequestParam("id") int id) throws JsonProcessingException {
		List<Employee> list = new ArrayList<Employee>();
		String ObjectEmployee = "OKE";
		managerEmployeeService.deleteEmployee(id);
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectEmployee = objectMapper.writeValueAsString(list);
		return ObjectEmployee;
	}
	@RequestMapping("/delete-select")
	public @ResponseBody String deletetest(@RequestParam("vcl[]") List<String> id) throws JsonProcessingException {
		String ObjectEmployee = "";
		List<String> list = new ArrayList<String>();
		for(String i : list) {
			list.add(i);
		}
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectEmployee = objectMapper.writeValueAsString(list);
		System.out.println(ObjectEmployee);
		return ObjectEmployee;
	}
}
