$(document).ready(function(){
	toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": false,
		  "positionClass": "toast-bottom-right",
		  "preventDuplicates": false,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
	}
    var name = '';
    var email = '';
    var address = '';
    var phone = '';
    
    var getListEmployee = function(){
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/fashionshop/manager/get-all-employee",
            dataType: "JSON",
            success: function (response) {
               var row = '';
                $.each(response, function (k,v) {

                    row = row + "<tr>";
                    row = row + "<td><span class='custom-checkbox'><input type='checkbox' name='test' value='"+v.id+"'>";
                    row = row +"<label></label></span></td>";
                    row = row+ "<td>"+v.name+"</td>";
                    row = row+ "<td>"+v.email+"</td>";
                    row = row+ "<td>"+v.address+"</td>";
                    row = row+ "<td>"+v.phone+"</td>";
                    row = row+ "<td>";
                    row = row+ "<button type='button' class='btn btn-danger sang_delete' data-id='"+v.id+"'>Delete</button>";
                    row = row+ "</td>";
                    row = row+ "</tr>";
                });
                $(".table tbody").html(row);
            }       
        });
        
    }
    getListEmployee();

    $("#btn_add").click(function () {
        var flag = true;
        name = $("#name_add").val();
        email = $("#email_add").val();
        address = $("#address_add").val();
        phone = $("#phone_add").val();
        error_name = $("#error_name");
        error_email = $("#error_email");
        error_address = $("#error_address");
        error_phone = $("#error_phone");
        var checkInputEmpty = function(value_input,value_error){
            if(value_input == ''){
              value_error.removeAttr("style","display:none");
              value_error.attr("style","color:red");
            }else{
              value_error.attr("style","display:none");
            }
        }
            checkInputEmpty(name,error_name);
            checkInputEmpty(email,error_email);
            checkInputEmpty(address,error_address);
            checkInputEmpty(phone,error_phone);
            if(name != '' && email !=  '' && address != '' && phone != ''){
                $.ajax({
                    type: "POST",
                    url: "http://localhost:8080/fashionshop/manager/add-employee",
                    data: {
                        name : name,
                        email : email,
                        address : address,
                        phone : phone,
                    },
                    dataType: "JSON",
                    success: function (response) {
                        if(response != ''){
                        	toastr.success('Add employee successful');
                            getListEmployee();
                        }else{
                        	toastr.warning('Email already exists');
                        }
                        
                    }       
                });
                $('#addEmployeeModal').modal('hide');
            }
    });

    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    $("#deleteAll").click(function(){
        var favorite = new Array();
        $.each($("input[name='test']:checked"),function(){
            favorite.push($(this).val());
        });
        console.log(favorite);
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/fashionshop/manager/delete-select",
            data: {
                vcl : favorite,
            },
            success: function (response) {
                console.log(response);
            }
        });
    });

    $("body").on("click",".sang_delete",function(){


        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/fashionshop/manager/delete-employee",
            data: {
                id : id,
            },
            dataType: "JSON",
            success: function (response) {
                console.log(response);
                if(response == '' || response.length == 0){
                    toastr.success('Delete employee successful');
                    getListEmployee();
                }
               
            }
        });
    });
	// Activate tooltip
	$('[data-toggle="tooltip"]').tooltip();
	
	// Select/Deselect checkboxes
	var checkbox = $('table tbody input[type="checkbox"]');
/* 	$("#selectAll").click(function(){
		if(this.checked){
			checkbox.each(function(){
				this.checked = true;                        
			});
		} else{
			checkbox.each(function(){
				this.checked = false;                        
			});
		} 
	}); */
/* 	checkbox.click(function(){
		if(!this.checked){
			$("#selectAll").prop("checked", false);
		}
    }); */

});
